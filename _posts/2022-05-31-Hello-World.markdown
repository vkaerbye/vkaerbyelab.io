---
layout: post
title:  "Hello World!"
date:   2022-05-31
categories: jekyll update
---

Hello World!

{% highlight python %}
i = 0
for i in range(0,10):
    i += 1
print(i)
{% endhighlight %}